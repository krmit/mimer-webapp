# Mimer webapp

En samling av några webbapp som kanske kan vara till nytta. Varje 
webbapp består av en html sida.

## Diverse

- [Klocka](./clock.html)
- [Konvertera från f till c](./convert-from-f-to-c.html)
- [Konvertera från USK to SEK](./convert-from-usd-to-sek.html)
- [Lägg till en kaka](./cookie.html)
- [Listar alla kakor på sajten](./list-of-cookies.html)

## Mimer Game Server

- [Gissa talet](./guess-number.html)
- [Förenklat Nim spel](./simpel-nim.html)

## Ljud

- [🚧 Spela en låt](./play-sound.html)

## Mallar
